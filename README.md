This is a simple address book, with no authentication, designed for a doctor to store details of his patients and staff.

### Installation

Requires MySQL, PHP5 and a web server.

- Create a new MySQL database
- Copy the address_book files to the web server root
- Import schema.sql into the database
- Edit the database settings in config.php