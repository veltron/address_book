<?php

include 'models/Contact.model.php';

class IndexController {

    // Constants representing the relevant page to be displayed
    const PLAIN = 0;
    const DELETED_CONTACT = 1;

    public function __construct()
    {
        $this->loadView($this->getRoute());
    }


    private function getRoute()
    {
        if (isset($_GET['deleted'])) {
            return self::DELETED_CONTACT;
        }
        return self::PLAIN;
    }

    private function loadView($route)
    {
        switch($route) {
            case self::DELETED_CONTACT:
                $contentFile = 'Index.view.php';
                $title = 'Contact Deleted';
                $heading = 'Contact Deleted';
                break;
            case self::PLAIN:
                $contentFile = 'Index.view.php';
                $title = 'Address Book Index';
                $heading = 'Address Book Index';
        }



        $db = new Database();
        $results = $db->fetch('Contacts', 'ContactId', '1 ORDER BY Surname ASC');

        if(count($results) > 0) {
            for($i = 0; $i <= (count($results) - 1); $i++) {
                $contacts[$i] = new Contact();
                $contacts[$i]->init($results[$i]['ContactId']);
            }
        }

        include 'views/Layout.view.php';
    }
}