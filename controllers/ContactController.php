<?php

include 'models/Contact.model.php';

class ContactController
{

    // Constants representing the relevant page to be displayed
    const UNKNOWN = 0;
    const SHOW_CONTACT = 1;
    const ADD_CONTACT = 2;
    const SUBMITTED_CONTACT = 3;
    const ADD_PHONE_NUMBER = 4;
    const SUBMITTED_PHONE_NUMBER = 5;
    const UPDATE_ADDRESS = 6;
    const ADDRESS_UPDATED = 7;
    const DELETE_PHONE = 8;

    private $contactId;
    private $error = false;

    public function __construct()
    {
        if(isset($_GET['id'])) {
            $this->contactId = $_GET['id'];
        }
        $this->loadView($this->getRoute());
    }

    private function getRoute()
    {
        if(isset($_GET['action'])) {
            switch($_GET['action']) {
                case 'view':
                    return self::SHOW_CONTACT;
                case 'add':
                    if(isset($_POST['send'])) {
                        $contact = new Contact();
                        $formDetails = $_POST;
                        unset($formDetails['send']);
                        $this->contactId = $contact->create($formDetails);
                        return self::SUBMITTED_CONTACT;
                    } else {
                        return self::ADD_CONTACT;
                    }
                case 'addPhone':
                    if(isset($_POST['send'])){
                        $phone = new PhoneNumber();
                        $formDetails = $_POST;
                        if(!$phone->create($formDetails['Number'], $this->contactId, $formDetails['Type'])) {
                            $this->error = true;
                        }
                        return self::SUBMITTED_PHONE_NUMBER;

                    } else {
                        return self::ADD_PHONE_NUMBER;
                    }
                case 'deletePhone':
                    $phone = new PhoneNumber();
                    $phone->init($_GET['phoneId']);
                    $phone->delete();
                    return self::DELETE_PHONE;
                case 'edit':
                    if(isset($_POST['send'])) {
                        $address = new Address($this->contactId);
                        $formDetails = $_POST;
                        unset($formDetails['send']);
                        if(!$address->update($formDetails)) {
                            $this->error = true;
                        }
                        return self::ADDRESS_UPDATED;
                    } else {
                        return self::UPDATE_ADDRESS;
                    }
                case 'delete':                                  // Doesn't return as it loads the index page and view is
                    $contact = new Contact();                   // handled there.
                    $contact->init($this->contactId);
                    $contact->delete();
                    header('Location:index.php?deleted=true');
                default:
                    return self::UNKNOWN;
            }
        }
        return self::UNKNOWN;
    }

    private function loadView($route)
    {
        if ($this->contactId) {
            $contact = new Contact();
            $contact->init($this->contactId);
        }
        switch($route) {
            case self::SHOW_CONTACT:
                $contentFile = 'ContactSingle.view.php';
                $title = 'Contact Details';
                $heading = $contact->getFirstName() . ' ' . $contact->getSurname();
                break;
            case self::ADD_CONTACT:
                $contentFile = 'ContactAdd.view.php';
                $title = 'Add New Contact';
                $heading = 'Add New Contact';
                break;
            case self::SUBMITTED_CONTACT:
                if (!$this->contactId) {
                    $contentFile = 'ContactAdd.view.php';
                    $title = 'Add New Contact';
                    $heading = 'Contact was not added';
                } else {
                    $contentFile = 'ContactSingle.view.php';
                    $title = 'Contact Added';
                    $heading = 'Contact was added successfully';
                }
                break;
            case self::ADD_PHONE_NUMBER;
                $contentFile = 'PhoneNumberAdd.view.php';
                $title = 'Add Phone Number';
                $heading = 'Add Phone Number';
                break;
            case self::SUBMITTED_PHONE_NUMBER:
                if(!$this->error) {
                    $contentFile = 'ContactSingle.view.php';
                    $title = 'Phone number added';
                    $heading = 'Phone number was added successfully';
                } else {
                    $contentFile = 'PhoneNumberAdd.view.php';
                    $title = 'Phone number not added';
                    $heading = 'Phone number was not added';
                }
                break;
            case self::DELETE_PHONE:
                $contentFile = 'ContactSingle.view.php';
                $title = 'Phone number deleted';
                $heading = 'Phone number was deleted successfully';
                break;
            case self::UPDATE_ADDRESS;
                $contentFile = 'UpdateAddress.view.php';
                $title = 'Update Address';
                $heading = 'Update Address';
                break;
            case self::ADDRESS_UPDATED:
                if(!$this->error) {
                    $contentFile = 'ContactSingle.view.php';
                    $title = 'Address Updated';
                    $heading = 'Address was updated successfully';
                } else {
                    $contentFile = 'PhoneNumberAdd.view.php';
                    $title = 'Address not updated';
                    $heading = 'Address was not updated';
                }
                break;
            case self::UNKNOWN:
                $contentFile = 'Unknown.view.php';
                $title = 'Page Not Found';
                $heading = 'Page Not Found';

        }
        include 'views/Layout.view.php';
    }
}