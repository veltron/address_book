<?php

include 'models/Search.model.php';

class SearchController
{
    public function __construct()
    {
        $title = 'Search Contacts';
        $heading = 'Search Contacts';

        if(isset($_GET['send'])) {
            $search = new SearchModel();
            $searchTerms = $_GET;
            unset($searchTerms['send']);
            $contacts = $search->search($searchTerms);
            $contentFile = 'SearchResults.view.php';
        } else {
            $contentFile = 'Search.view.php';
        }
        include 'views/Layout.view.php';
    }
}


