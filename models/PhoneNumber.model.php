<?php

include_once 'Database.php';

class PhoneNumber
{

    const TYPE_HOME = 0;
    const TYPE_WORK = 1;
    const TYPE_MOBILE = 2;

    private $table = 'PhoneNumbers';
    private $number;
    private $initialised = false;
    private $type;
    private $id;

    public function init($phoneNumberId)
    {
        $this->id = $phoneNumberId;

        $db = new Database();
        $phoneNumberId = $db->escape($phoneNumberId);
        $result = $db->fetchSingle($this->table, 'Number, Type', "PhoneNumberId = '$phoneNumberId'");
        $this->number = $result['Number'];
        $this->type = $result['Type'];
        $this->initialised = true;
    }

    public function getNumber()
    {
        return $this->number;
    }

    public function create($number, $contactId, $type)
    {
        $db = new Database();
        $validator = new Validation();
        $number = $db->escape($number);
        $contactId = $db->escape($contactId);
        $type = $db->escape($type);

        if($validator->isNumeric($number) && $validator->lengthIs($number, 11)) {
            $data = array ('ContactId' => $contactId, 'Number' => $number, 'Type' => $type);
            $data = $db->escapeArray($data);
            if ($db->insert($this->table, $data)) {
                $this->initialised = true;
                return true;
            }
        }
        return false;
    }

    public function delete()
    {
        $db = new Database();
        return $db->delete($this->table, 'PhoneNumberId =' . $this->id);
    }

    public function getType() {
        return $this->type;
    }

    public function getTypeAsString() {
        $type = $this->getType();

        switch($type) {
            case self::TYPE_HOME:
                return 'Home';
            case self::TYPE_WORK:
                return 'Work';
            case self::TYPE_MOBILE:
                return 'Mobile';
        }
    }

    public function getId()
    {
        return $this->id;
    }
}