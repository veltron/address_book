<?php

class Database
{

    private $connection;

    public function __construct()
    {
        include(dirname(__FILE__) . DIRECTORY_SEPARATOR . '..' . DIRECTORY_SEPARATOR . 'config.php');
        $this->connection = new mysqli($dbHost, $dbUser, $dbPassword, $dbName);
    }

    public function insert($table, $data)
    {
        $queryStart = 'INSERT INTO ' . $table . '(';
        $queryEnd = ' VALUES (';

        foreach($data as $column => $value) {
            $queryStart = $queryStart . $column . ', ';
            $queryEnd = $queryEnd . '"' . $value . '" , ';
        }

        $queryStart = substr($queryStart, 0, strlen($queryStart) - 2) . ')';
        $queryEnd = substr($queryEnd, 0, strlen($queryEnd) - 2) . ');';

        $this->query($queryStart . $queryEnd);
        return $this->connection->insert_id;

    }

    public function update($table, $data, $condition)
    {
        $query = 'UPDATE ' . $table . ' SET ';

        foreach($data as $column => $value) {
            $query = "$query $column='$value',";
        }

        $query = substr($query, 0, strlen($query) - 1);
        $query = $query . " WHERE $condition;";
        return $this->query($query);
    }

    public function delete ($table, $condition)
    {
        $query = 'DELETE FROM ' . $table . ' WHERE ' . $condition;
        return $this->query($query);
    }

    private function query($sql)
    {
        return $this->connection->query($sql);
    }

    public function escape($string)
    {
        return $this->connection->real_escape_string($string);
    }

    public function escapeArray($array) {
        foreach ($array as $key => $value) {
            $array[$key] = $this->escape($value);
        }
        return $array;
    }

    public function fetch($table, $columns, $condition)
    {

        $query = 'SELECT';

        if ($columns == '*') {
            $query = $query . ' *';
        } elseif (!is_array($columns)) {
            $query = $query . ' ' . $columns;
        } else {
            foreach ($columns as $column) {
                $query = $query . ' ' . $column . ',';
            }
            $query = substr($query, 0, strlen($query) - 1);
        }

        $query = $query . ' FROM ' . $table .  ' WHERE ' . $condition . ';';
        $result = $this->query($query);

        if ($result) {
            while ($row = $result->fetch_assoc()) {
                $results_array[] = $row;
            }
            return $results_array;
        } else {
            return false;
        }
    }

    public function fetchSingle($table, $columns, $condition)
    {
        $result = $this->fetch($table, $columns, $condition);
        if(!$result) {
            return false;
        }
        $result = $result[0];
        return $result;
    }
}