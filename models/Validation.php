<?php

class Validation
{

    public function missingValues($data, $required)
    {
        $missingFields = array();
        foreach($data as $field => $value) {
            foreach($required as $requiredField) {
                if(($field == $requiredField) && !$value) {
                    array_push($missingFields, $requiredField);
                }
            }
        }
    }

    public function isNumeric($input) {
        return is_numeric($input);
    }

    public function isAlphaNumeric($input) {
        return ctype_alnum($this->removeSpaces($input));
    }

    public function isAlphabetical($input) {
        return ctype_alpha($this->removeSpaces($input));
    }

    public function lengthIs($input, $length) {
        if(strlen($input) == $length) {
            return true;
        }
        return false;
    }

    public function removeSpaces($input)
    {
        return str_replace(' ', '', $input);
    }
}