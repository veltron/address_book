<?php

include_once 'Database.php';

class Address
{

    private $table ='Addresses';
    private $addressId;
    private $contactId;
    private $addressLine1;
    private $addressLine2;
    private $town;
    private $county;
    private $postCode;

    public function __construct($contactId)
    {
        $db = new Database();
        $result = $db->fetchSingle($this->table, '*', 'ContactId = ' . $db->escape($contactId));

        if ($result) {
            $this->addressId = $result['AddressId'];
            $this->contactId = $result['ContactId'];
            $this->addressLine1 = $result['AddressLine1'];
            $this->addressLine2 = $result['AddressLine2'];
            $this->town = $result['Town'];
            $this->county = $result['County'];
            $this->postCode = $result['PostCode'];
        }
    }

    public function delete()
    {
        $db = new Database();
        $db->delete($this->table, "AddressId = '$this->addressId'");
    }

    public function update($data)
    {
        $requiredFields = array( 'AddressLine1', 'Town', 'County', 'PostCode');
        $validator = new Validation();

        if($validator->missingValues($data, $requiredFields)) {
            return false;
        }

        if($validator->isAlphaNumeric($data['AddressLine1']) && $validator->isAlphabetical($data['Town']) &&
            $validator->isAlphabetical($data['County']) && $validator->isAlphaNumeric($data['PostCode'])) {
            $db = new Database();
            $data = $db->escapeArray($data);
            return $db->update($this->table, $data, "AddressId = '$this->addressId'");
        }
        return false;
    }

    public function getAddress()
    {
        return array('Address Line 1' => $this->addressLine1,
                        'Address Line 2' => $this->addressLine2,
                        'Town' => $this->town,
                        'County' => $this->county,
                        'Post Code' => $this->postCode
        );
    }
}