<?php

include_once 'Database.php';
include 'Address.model.php';
include 'PhoneNumber.model.php';
include 'Validation.php';

/**
 * Class Contact
 *
 * This class is not initialised on instantiation to allow it to be used to create a new entry.
 * The init method should always be called if retrieving a existing record.
 */
class Contact
{

    const CONTACT_TYPE_STAFF = 0;
    const CONTACT_TYPE_PATIENT = 1;
    const CONTACT_TYPE_PERSONAL = 2;

    private $table = 'Contacts';
    private $initialised = false;
    private $contactId;
    private $firstName;
    private $surname;
    private $type;

    public $address;
    public $phoneNumbers;

    public function init($contactId)
    {
        $this->contactId = $contactId;
        $db = new Database();
        $contactId = $db->escape($contactId);
        $contactData = $db->fetchSingle($this->table, 'FirstName, Surname, Type', "ContactId = '$contactId'");

        if (!$contactData) {
            return false;
        }

        $this->firstName = $contactData['FirstName'];
        $this->surname = $contactData['Surname'];
        $this->type = $contactData['Type'];

        $this->address = new Address($contactId);
        $this->phoneNumbers = $this->getPhoneNumbers();
    }

    public function getFirstName()
    {
        return $this->firstName;
    }

    public function getSurname()
    {
        return $this->surname;
    }

    public function getType()
    {
        return $this->type;
    }

    public function delete()
    {
        $db = new Database();
        $db->delete($this->table, "ContactId = '$this->contactId'");

        if (count($this->phoneNumbers) > 0) {
            foreach($this->phoneNumbers as $phoneNumber) {
                $phoneNumber->delete();
            }
        }
        $this->address->delete();
    }

    /**
     * @param $data An associative array of fields required in the database
     * @return bool|mixed If successful returns the newly created Contacts ID, if it fails false is returned.
     *
     * As the Address and contact are dependant on each other it makes sense to insert them into the database
     * together.
     */
    public function create($data)
    {
        if (!$this->initialised)
        {
            $requiredFields = array( 'FirstName', 'Surname', 'Type', 'AddressLine1', 'Town', 'County', 'PostCode');
            $db = new Database();
            $validator = new Validation();

            if($validator->missingValues($data, $requiredFields)) {
                return false;
            }

            if ($validator->isAlphabetical($data['FirstName']) && $validator->isNumeric($data['Type']) &&
                $validator->isAlphaNumeric($data['Type']) && $validator->isAlphaNumeric($data['AddressLine1']) &&
                $validator->isAlphabetical($data['Town']) && $validator->isAlphabetical($data['County']) &&
                $validator->isAlphaNumeric($data['PostCode'])) {

                $data = $db->escapeArray($data);
                $contactFields = $data;
                unset($contactFields['AddressLine1']);
                unset($contactFields['AddressLine2']);
                unset($contactFields['Town']);
                unset($contactFields['County']);
                unset($contactFields['PostCode']);

                $addressFields = $data;
                unset($addressFields['FirstName']);
                unset($addressFields['Surname']);
                unset($addressFields['Type']);

                $this->contactId = $db->insert($this->table, $contactFields);
                $addressFields['ContactId'] = $this->contactId;
                $db->insert('Addresses', $addressFields);
                $this->init($this->contactId);

                return $this->contactId;
            }
        } else {
            return false;
        }
    }

    private function getPhoneNumbers()
    {
        $db = new Database();
        $result = $db->fetch('PhoneNumbers', array('PhoneNumberId') , 'ContactId = ' . $this->contactId . ' ORDER BY Type ASC');

        if ($result) {
            $phoneNumberArray = null;
            $i = 0;
            foreach($result as $row => $single) {
                $phoneNumberArray[$i] = new PhoneNumber();
                $phoneNumberArray[$i]->init($single['PhoneNumberId']);
                $i++;
            }
            return $phoneNumberArray;
        }
    }

    public function getId()
    {
        return $this->contactId;
    }

    public function getTypeAsString()
    {
        switch($this->type) {
            case self::CONTACT_TYPE_PATIENT:
                return 'Patient';
            case self::CONTACT_TYPE_STAFF:
                return 'Staff';
            case self::CONTACT_TYPE_PERSONAL:
                return 'Personal';
        }
    }
}