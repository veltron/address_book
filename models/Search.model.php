<?php

include_once 'Database.php';
include 'Contact.model.php';

class SearchModel
{

    public function search($data)
    {
        $conditions = '';
        $db = new Database();

        if($data['Type'] > 2) {
            unset($data['Type']);
        }

        foreach($data as $column => $value) {
            if($value || $column == 'Type') {   // Stop Type 0 being missed
                $conditions = $conditions . 'LOWER(' . $db->escape($column) . ') = ' . "'" .
                    strtolower($db->escape($value)) ."' AND ";
            }
        }

        $conditions = substr($conditions, 0, strlen($conditions) - 5);
        $results = $db->fetch('Contacts', 'ContactId', $conditions);

        if($results) {
            $contacts = array();

            for($i = 0; $i <= (count($results) -1); $i++) {
                $contacts[$i] = new Contact();
                $contacts[$i]->init($results[$i]['ContactId']);
            }

            return $contacts;
        } else {
            return false;
        }
    }
}