<?php
    if (!$contacts) {
        echo 'No contacts were found please search again.';
        include 'Search.view.php';
        exit;

    }
?>

<table class="pure-table pure-table-horizontal">
    <thead>
        <tr>
            <th>First Name</th>
            <th>Surname</th>
        </tr>
    </thead>
    <tbody>
    <?php
        if($contacts) {
            foreach($contacts as $contact) {
                echo '<tr><td><a href="contact.php?action=view&amp;id=' . $contact->getId() . '">' . $contact->getFirstName() . '</a></td>';
                echo '<td><a href="contact.php?action=view&amp;id=' . $contact->getId() . '">' . $contact->getSurname() . '</a></td></tr>';
            }
        }
    ?>
    </tbody>
</table>