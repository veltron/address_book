<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <link rel="stylesheet" href="http://yui.yahooapis.com/pure/0.5.0/pure-min.css" />
    <link rel="stylesheet" href="style.css" />
    <title><?php echo $title ?></title>
</head>
<body>
    <h1><?php echo $heading ?></h1>

    <div class="pure-menu pure-menu-open pure-menu-horizontal">
        <ul>
            <li><a href="./">Index</a></li>
            <li><a href="./search.php">Search</a></li>
            <li><a href="./contact.php?action=add">Add Contact</a></li>
        </ul>
    </div>

    <?php
    if (isset($contentFile)) {
        include($contentFile);
    } ?>

</body>
</html>