<form class="pure-form pure-form-aligned" method="get" action="search.php">
    <fieldset>
        <div class="pure-control-group">
            <label for="firstname">First Name</label>
            <input id="firstname" name="FirstName" type="text" />
        </div>

        <div class="pure-control-group">
            <label for="surname">Surname</label>
            <input id="surname" name="Surname" type="text" />
        </div>

        <label for="type">Category</label>
        <select id="type" name="Type">
            <option value="0">Staff</option>
            <option value="1">Patient</option>
            <option value="2">Personal</option>
            <option value="3">All</option>
        </select>

        <div class="pure-controls">
            <button name="send" type="submit" class="pure-button pure-button-primary" value="submit">Search</button>
        </div>
    </fieldset>
</form>