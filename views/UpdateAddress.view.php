<form class="pure-form pure-form-aligned" method="post" action="contact.php?id=<?php echo $contact->getId();?>&amp;action=edit">
    <fieldset>

        <div class="pure-control-group">
            <label for="address1">Address Line 1</label>
            <input id="address1" name="AddressLine1" type="text" />
        </div>

        <div class="pure-control-group">
            <label for="address2">Address Line 2</label>
            <input id="address2" name="AddressLine2" type="text" />
        </div>

        <div class="pure-control-group">
            <label for="town">Town</label>
            <input id="town" name="Town" type="text" />
        </div>

        <div class="pure-control-group">
            <label for="county">County</label>
            <input id="county" name="County" type="text" />
        </div>

        <div class="pure-control-group">
            <label for="postcode">Post Code</label>
            <input id="postcode" name="PostCode" type="text" />
        </div>

        <div class="pure-controls">
            <button name="send" type="submit" class="pure-button pure-button-primary" value="submit">Submit</button>
        </div>
    </fieldset>
</form>