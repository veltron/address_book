<h2>Details</h2>
<table class="pure-table pure-table-horizontal">
    <tr><td>First Name</td>
    <?php echo '<td>' . $contact->getFirstName() . '</td></tr>' . PHP_EOL; ?>
    <tr><td>Surname</td>
    <?php
        echo '<td>' . $contact->getSurname() . '</td></tr>' . PHP_EOL;
        foreach($contact->address->getAddress() as $key => $value) {
            echo '<tr><td>' . $key . '</td>' . '<td>' . $value . '</td></tr>' . PHP_EOL;
        }
        echo '<tr><td>Type</td><td>' . $contact->getTypeAsString() . '</td></tr>';
    ?>
</table>

<?php
    if(count($contact->phoneNumbers) > 0) {
        include 'PhoneNumberDisplay.php';
    }
?>

<div class="pure-menu pure-menu-open pure-menu-horizontal">
    <ul>
        <li><a href="./contact.php?id=<?php echo $contact->getId(); ?>&amp;action=addPhone">Add Phone Number</a></li>
        <li><a href="./contact.php?id=<?php echo $contact->getId(); ?>&amp;action=edit">Edit Address</a></li>
        <li><a href="./contact.php?id=<?php echo $contact->getId(); ?>&amp;action=delete">Delete Contact</a></li>
    </ul>
</div>